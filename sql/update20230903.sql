DROP TABLE IF EXISTS `dberp_plugin`;
CREATE TABLE IF NOT EXISTS `dberp_plugin` (
    `plugin_id` int(11) NOT NULL AUTO_INCREMENT,
    `plugin_name` varchar(100) NOT NULL,
    `plugin_author` varchar(100) NOT NULL,
    `plugin_author_url` varchar(200) DEFAULT NULL,
    `plugin_info` text NOT NULL,
    `plugin_version` varchar(20) NOT NULL,
    `plugin_version_num` int(11) NOT NULL,
    `plugin_code` varchar(50) NOT NULL,
    `plugin_state` tinyint(1) NOT NULL DEFAULT '0',
    `plugin_support_url` varchar(200) DEFAULT NULL,
    `plugin_admin_path` varchar(200) DEFAULT NULL,
    `plugin_update_time` date NOT NULL,
    PRIMARY KEY (`plugin_id`),
    KEY `plugin_version_num` (`plugin_version_num`),
    KEY `plugin_code` (`plugin_code`),
    KEY `plugin_state` (`plugin_state`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='插件表';

ALTER TABLE `dberp_accounts_receivable` ADD `sales_invoice_id` INT NOT NULL AFTER `finish_amount`, ADD INDEX (`sales_invoice_id`);
ALTER TABLE `dberp_accounts_receivable` CHANGE `sales_invoice_id` `sales_invoice_id` INT(11) NULL DEFAULT '0';

ALTER TABLE `dberp_finance_payable` ADD `purchase_invoice_id` INT NULL DEFAULT '0' AFTER `finish_amount`, ADD INDEX (`purchase_invoice_id`);