<?php
/**
 * DBERP 进销存系统
 *
 * ==========================================================================
 * @link      http://www.dberp.net/
 * @copyright 北京珑大钜商科技有限公司，并保留所有权利。
 * @license   http://www.dberp.net/license.html License
 * ==========================================================================
 *
 * @author    静静的风 <baron@loongdom.cn>
 *
 */

namespace Purchase\Repository;

use Doctrine\ORM\EntityRepository;
use Purchase\Entity\OrderGoods;

class OrderGoodsRepository extends EntityRepository
{
    /**
     * 根据orderId获取列表信息
     * @param array $orderId
     * @return float|int|mixed|string
     */
    public function findPurchaseOrderIdList(array $orderId)
    {
        $query = $this->getEntityManager()->createQueryBuilder();
        $query->select('g')
            ->from(OrderGoods::class, 'g')
            ->where($query->expr()->in('g.pOrderId', $orderId));

        return $query->getQuery()->getResult();
    }
}