<?php
/**
 * DBERP 进销存系统
 *
 * ==========================================================================
 * @link      http://www.dberp.net/
 * @copyright 北京珑大钜商科技有限公司，并保留所有权利。
 * @license   http://www.dberp.net/license.html License
 * ==========================================================================
 *
 * @author    静静的风 <baron@loongdom.cn>
 *
 */

namespace Report;

use Report\Controller\IndexController;
use Report\Controller\ReportStockController;

return [
    'Report' => [
        'name' => '报表',
        'controllers' => [
            ReportStockController::class => [
                'name' => '库存报表',
                'action' => ['index'],
                'actionNames' => [
                    'index' => '查看库存报表'
                ]
            ],
            IndexController::class => [
                'name' => '更多报表',
                'action' => ['index'],
                'actionNames' => [
                    'index' => '查看更多报表'
                ]
            ]
        ]
    ]
];