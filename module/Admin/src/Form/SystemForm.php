<?php
/**
 * DBERP 进销存系统
 *
 * ==========================================================================
 * @link      http://www.dberp.net/
 * @copyright 北京珑大钜商科技有限公司，并保留所有权利。
 * @license   http://www.dberp.net/license.html License
 * ==========================================================================
 *
 * @author    静静的风 <baron@loongdom.cn>
 *
 */

namespace Admin\Form;

use Laminas\Form\Form;

class SystemForm extends Form
{
    public function __construct($name = 'system-form', array $options = [])
    {
        parent::__construct($name, $options);

        $this->setAttribute('method', 'post');
        $this->setAttribute('class', 'form-horizontal');

        $this->addElements();
        $this->addInputFilter();
    }

    public function addElements()
    {
        $this->add([
            'type'  => 'text',
            'name'  => 'company_name|base',
            'attributes'    => [
                'id'            => 'company_name|base',
                'class'         => 'form-control'
            ]
        ]);

        $this->add([
            'type'  => 'text',
            'name'  => 'website_icp|base',
            'attributes'    => [
                'id'            => 'website_icp|base',
                'class'         => 'form-control'
            ]
        ]);

        $this->add([
            'type'  => 'checkbox',
            'name'  => 'shop_service|base',
            'attributes' => [
                'value' => 1
            ]
        ]);

        $this->add([
            'type'  => 'select',
            'name'  => 'currency_code|base',
            'attributes'    => [
                'id'        => 'currency_code|base',
                'class'     => 'form-control select2'
            ]
        ]);

        $this->add([
            'type'  => 'select',
            'name'  => 'website_timezone|base',
            'attributes'    => [
                'id'            => 'website_timezone|base',
                'class'         => 'form-control'
            ]
        ]);

        $this->add([
            'type'  => 'text',
            'name'  => 'company_tax_number|base',
            'attributes'    => [
                'id'            => 'company_tax_number|base',
                'class'         => 'form-control'
            ]
        ]);
        $this->add([
            'type'  => 'text',
            'name'  => 'company_address|base',
            'attributes'    => [
                'id'            => 'company_address|base',
                'class'         => 'form-control'
            ]
        ]);
        $this->add([
            'type'  => 'text',
            'name'  => 'company_phone|base',
            'attributes'    => [
                'id'            => 'company_phone|base',
                'class'         => 'form-control'
            ]
        ]);
        $this->add([
            'type'  => 'text',
            'name'  => 'company_bank|base',
            'attributes'    => [
                'id'            => 'company_bank|base',
                'class'         => 'form-control'
            ]
        ]);
        $this->add([
            'type'  => 'text',
            'name'  => 'company_bank_account|base',
            'attributes'    => [
                'id'            => 'company_bank_account|base',
                'class'         => 'form-control'
            ]
        ]);

        $this->add([
            'type'  => 'checkbox',
            'name'  => 'login_captcha|other',
            'attributes' => [
                'value' => 0
            ]
        ]);

        $this->add([
            'type'  => 'number',
            'name'  => 'stock_min_warning|other',
            'attributes'    => [
                'id'    => 'stock_min_warning|other',
                'class' => 'form-control',
                'value' => 0
            ]
        ]);

        $this->add([
            'type'  => 'number',
            'name'  => 'stock_max_warning|other',
            'attributes'    => [
                'id'    => 'stock_max_warning|other',
                'class' => 'form-control',
                'value' => 0
            ]
        ]);

        $this->add([
            'type'  => 'number',
            'name'  => 'image_width|other',
            'attributes'    => [
                'id'    => 'image_width|other',
                'class' => 'form-control',
                'value' => 50
            ]
        ]);
    }

    public function addInputFilter()
    {
        $inputFilter = $this->getInputFilter();

        $inputFilter->add([
            'name'      => 'company_name|base',
            'required'  => true,
            'filters'   => [
                ['name' => 'StringTrim'],
                ['name' => 'StripTags']
            ]
        ]);

        $inputFilter->add([
            'name'      => 'website_icp|base',
            'required'  => false,
            'filters'   => [
                ['name' => 'StringTrim'],
                ['name' => 'StripTags']
            ]
        ]);

        $inputFilter->add([
            'name'      => 'shop_service|base',
            'required'  => false,
            'validators'=> [
                [
                    'name'      => 'InArray',
                    'options'   => [
                        'haystack'  => [0, 1]
                    ]
                ]
            ]
        ]);

        $inputFilter->add([
            'name'      => 'currency_code|base',
            'required'  => true,
            'filters'   => [
                ['name' => 'StringTrim'],
                ['name' => 'StripTags']
            ]
        ]);

        $inputFilter->add([
            'name'      => 'website_timezone|base',
            'required'  => false
        ]);

        $inputFilter->add([
            'name'      => 'company_tax_number|base',
            'required'  => false,
            'filters'   => [
                ['name' => 'StringTrim'],
                ['name' => 'StripTags']
            ]
        ]);
        $inputFilter->add([
            'name'      => 'company_address|base',
            'required'  => false,
            'filters'   => [
                ['name' => 'StringTrim'],
                ['name' => 'StripTags']
            ]
        ]);
        $inputFilter->add([
            'name'      => 'company_phone|base',
            'required'  => false,
            'filters'   => [
                ['name' => 'StringTrim'],
                ['name' => 'StripTags']
            ]
        ]);
        $inputFilter->add([
            'name'      => 'company_bank|base',
            'required'  => false,
            'filters'   => [
                ['name' => 'StringTrim'],
                ['name' => 'StripTags']
            ]
        ]);
        $inputFilter->add([
            'name'      => 'company_bank_account|base',
            'required'  => false,
            'filters'   => [
                ['name' => 'StringTrim'],
                ['name' => 'StripTags']
            ]
        ]);

        $inputFilter->add([
            'name'      => 'login_captcha|other',
            'required'  => false,
            'validators'=> [
                [
                    'name'      => 'InArray',
                    'options'   => [
                        'haystack'  => [0, 1]
                    ]
                ]
            ]
        ]);

        $inputFilter->add([
            'name'      => 'image_width|other',
            'required'  => false,
            'filters'   => [
                ['name' => 'ToInt']
            ],
            'validators'=> [
                [
                    'name'      => 'GreaterThan',
                    'options'   => [
                        'min'   => 0
                    ]
                ]
            ]
        ]);
    }
}