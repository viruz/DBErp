<?php
/**
 * DBERP 进销存系统
 *
 * ==========================================================================
 * @link      http://www.dberp.net/
 * @copyright 北京珑大钜商科技有限公司，并保留所有权利。
 * @license   http://www.dberp.net/license.html License
 * ==========================================================================
 *
 * @author    静静的风 <baron@loongdom.cn>
 *
 */

namespace Admin\Service\Common;

use Admin\Data\Common;
use Laminas\I18n\View\Helper\CurrencyFormat;
use Laminas\Json\Json;

class PrintTemplate
{
    /**
     * 其他入库单数据输出
     * @param array $data
     * @param $templateBody
     * @return string
     */
    public static function otherInStock(array $data, $templateBody): string
    {
        $array = [
            'companyName'           => Common::configValue('base', 'config')['company_name'],
            'warehouseOrderSn'      => $data['otherWarehouseOrder']->getWarehouseOrderSn(),
            'otherAddTime'          => date("Y-m-d H:i:s", $data['otherWarehouseOrder']->getOtherAddTime()),
            'warehouseOrderState'   => $data['warehouseOrderState'],
            'warehouseOrderInfo'    => $data['otherWarehouseOrder']->getWarehouseOrderInfo(),
            'warehouseName'         => $data['otherWarehouseOrder']->getOneWarehouse()->getWarehouseName(),
            'warehouseOrderAmount'  => self::shopPrice($data['otherWarehouseOrder']->getWarehouseOrderAmount()),
        ];
        foreach ($data['orderGoods'] as $goodsValue) {
            $array['warehouseGoods'][] = [
                'goodsSn'       => $goodsValue->getGoodsNumber(),
                'goodsName'     => $goodsValue->getGoodsName(),
                'goodsSpec'     => $goodsValue->getGoodsSpec(),
                'goodsUnit'     => $goodsValue->getGoodsUnit(),
                'goodsPrice'    => self::shopPrice($goodsValue->getWarehouseGoodsPrice()),
                'goodsNum'      => $goodsValue->getWarehouseGoodsBuyNum(),
                'goodsAmount'   => self::shopPrice($goodsValue->getWarehouseGoodsAmount())
            ];
        }

        return self::outJson($templateBody, $array);
    }

    /**
     * 其他入库单数据输出
     * @param array $data
     * @param $templateBody
     * @return string
     */
    public static function otherOutStock(array $data, $templateBody): string
    {
        $array = [
            'companyName'       => Common::configValue('base', 'config')['company_name'],
            'exWarehouseOrderSn'=> $data['exWarehouseOrder']->getExWarehouseOrderSn(),
            'exAddTime'         => date("Y-m-d H:i:s", $data['exWarehouseOrder']->getExAddTime()),
            'exWarehouseOrderState' => $data['exWarehouseOrderState'],
            'exWarehouseOrderInfo'  => $data['exWarehouseOrder']->getExWarehouseOrderInfo(),
            'warehouseName'     => $data['exWarehouseOrder']->getOneWarehouse()->getWarehouseName(),
            'exWarehouseOrderAmount'=> self::shopPrice($data['exWarehouseOrder']->getExWarehouseOrderAmount()),
        ];

        foreach ($data['orderGoods'] as $goodsValue) {
            $array['warehouseGoods'][] = [
                'goodsSn'       => $goodsValue->getGoodsNumber(),
                'goodsName'     => $goodsValue->getGoodsName(),
                'goodsSpec'     => $goodsValue->getGoodsSpec(),
                'goodsUnit'     => $goodsValue->getGoodsUnit(),
                'goodsPrice'    => self::shopPrice($goodsValue->getWarehouseGoodsPrice()),
                'goodsNum'      => $goodsValue->getWarehouseGoodsExNum(),
                'goodsAmount'   => self::shopPrice($goodsValue->getWarehouseGoodsAmount())
            ];
        }

        return self::outJson($templateBody, $array);
    }

    /**
     * 库存盘点单数据输出
     * @param array $data
     * @param $templateBody
     * @return string
     */
    public static function stockCheck(array $data, $templateBody): string
    {
        $array = [
            'companyName'   => Common::configValue('base', 'config')['company_name'],
            'stockCheckSn'  => $data['stockCheckInfo']->getStockCheckSn(),
            'stockCheckTime'=> date("Y-m-d", $data['stockCheckInfo']->getStockCheckTime()),
            'stockCheckUser'=> $data['stockCheckInfo']->getStockCheckUser(),
            'stockCheckState'   => $data['stockCheckInfo']->getStockCheckState() == 1 ? '已盘点' : '待盘点',
            'stockCheckInfo'    => $data['stockCheckInfo']->getStockCheckInfo(),
            'warehouseName'     => $data['stockCheckInfo']->getOneWarehouse()->getWarehouseName(),
            'stockCheckAmount'  => self::shopPrice($data['stockCheckInfo']->getStockCheckAmount())
        ];

        foreach ($data['stockCheckGoods'] as $goodsValue) {
            $array['stockCheckGoods'][] = [
                'goodsSn'       => $goodsValue->getGoodsNumber(),
                'goodsName'     => $goodsValue->getGoodsName(),
                'goodsSpec'     => $goodsValue->getGoodsSpec(),
                'goodsUnit'     => $goodsValue->getGoodsUnit(),
                'stockCheckPreGoodsNum' => $goodsValue->getStockCheckPreGoodsNum(),
                'stockCheckAftGoodsNum' => $goodsValue->getStockCheckAftGoodsNum(),
                'stockCheckGoodsAmount' => self::shopPrice($goodsValue->getStockCheckGoodsAmount())
            ];
        }

        return self::outJson($templateBody, $array);
    }

    /**
     * 库间调拨单数据输出
     * @param array $data
     * @param $templateBody
     * @return string
     */
    public static function stockTransfer(array $data, $templateBody): string
    {
        $array = [
            'companyName' => Common::configValue('base', 'config')['company_name'],
            'transferSn'        => $data['transferInfo']->getTransferSn(),
            'transferAddTime'   => date("Y-m-d", $data['transferInfo']->getTransferAddTime()),
            'transferFinishTime'=> empty($data['transferInfo']->getTransferFinishTime()) ? '' : date("Y-m-d", $data['transferInfo']->getTransferFinishTime()),
            'transferInfo'      => $data['transferInfo']->getTransferInfo(),
            'outOneWarehouse'    => $data['transferInfo']->getOutOneWarehouse()->getWarehouseName(),
            'inOneWarehouse'    => $data['transferInfo']->getInOneWarehouse()->getWarehouseName(),
        ];

        foreach ($data['transferGoods'] as $goodsValue) {
            $array['transferGoods'][] = [
                'goodsSn'       => $goodsValue->getGoodsNumber(),
                'goodsName'     => $goodsValue->getGoodsName(),
                'goodsSpec'     => $goodsValue->getGoodsSpec(),
                'goodsUnit'     => $goodsValue->getGoodsUnit(),
                'transferGoodsNum' => $goodsValue->getTransferGoodsNum(),
                'transferGoodsState' => $data['stockHelper']->transferGoodsState($goodsValue->getTransferGoodsState(), 2)
            ];
        }

        return self::outJson($templateBody, $array);
    }

    /**
     * 采购单数据输出
     * @param array $data
     * @param $templateBody
     * @return string
     */
    public static function purchaseOrder(array $data, $templateBody): string
    {
        $array = [
            'companyName' => Common::configValue('base', 'config')['company_name'],
            'pOrderSn'  => $data['orderInfo']->getPOrderSn(),
            'paymentCode'  => $data['purchaseHelper']->orderPayment($data['orderInfo']->getPaymentCode()),
            'pOrderState'  => $data['purchaseHelper']->orderState($data['orderInfo']->getPOrderState(), 2),
            'supplierName'  => $data['orderInfo']->getOneSupplier()->getSupplierName(),
            'supplierContacts'  => $data['orderInfo']->getSupplierContacts(),
            'supplierPhone'  => $data['orderInfo']->getSupplierPhone(),
            'supplierTelephone'  => $data['orderInfo']->getSupplierTelephone(),
            'pOrderInfo'  => $data['orderInfo']->getPOrderInfo(),
            'pOrderAmount'  => self::shopPrice($data['orderInfo']->getPOrderAmount()),
        ];

        foreach ($data['orderGoods'] as $goodsValue) {
            $array['warehouseGoods'][] = [
                'goodsSn'       => $goodsValue->getGoodsNumber(),
                'goodsName'     => $goodsValue->getGoodsName(),
                'goodsSpec'     => $goodsValue->getGoodsSpec(),
                'goodsUnit'     => $goodsValue->getGoodsUnit(),
                'goodsPrice' => self::shopPrice($goodsValue->getPGoodsPrice()),
                'goodsNum' => $goodsValue->getPGoodsBuyNum(),
                'goodsTax' => self::shopPrice($goodsValue->getPGoodsTax()),
                'goodsAmount' => self::shopPrice($goodsValue->getPGoodsAmount()),
            ];
        }

        foreach ($data['purchaseOperLog'] as $value) {
            $array['purchaseOperLog'][] = [
                'orderState'=> $data['purchaseHelper']->orderState($value->getOrderState(), 2),
                'operUser'  => $value->getOperUser(),
                'operTime'  => date("Y-m-d H:i:s", $value->getOperTime())
            ];
        }

        return self::outJson($templateBody, $array);
    }

    /**
     * 采购退货单数据输出
     * @param array $data
     * @param $templateBody
     * @return string
     */
    public static function purchaseOrderReturn(array $data, $templateBody): string
    {
        $array = [
            'companyName' => Common::configValue('base', 'config')['company_name'],
            'pOrderSn' => $data['returnInfo']->getPOrderSn(),
            'supplierName' => $data['returnInfo']->getOnePOrder()->getOneSupplier()->getSupplierName(),
            'returnState' => $data['purchaseHelper']->orderReturnState($data['returnInfo']->getReturnState(), 2),
            'supplierContacts' => $data['returnInfo']->getOnePOrder()->getOneSupplier()->getSupplierContacts(),
            'supplierPhone' => $data['returnInfo']->getOnePOrder()->getOneSupplier()->getSupplierPhone(),
            'supplierTelephone' => $data['returnInfo']->getOnePOrder()->getOneSupplier()->getSupplierTelephone(),
            'pOrderReturnInfo' => $data['returnInfo']->getPOrderReturnInfo(),
            'pOrderReturnAmount' => self::shopPrice($data['returnInfo']->getPOrderReturnAmount()),
        ];

        foreach ($data['orderGoods'] as $goodsValue) {
            $array['warehouseGoods'][] = [
                'goodsSn'       => $goodsValue->getGoodsNumber(),
                'goodsName'     => $goodsValue->getGoodsName(),
                'goodsSpec'     => $goodsValue->getGoodsSpec(),
                'goodsUnit'     => $goodsValue->getGoodsUnit(),
                'goodsPrice' => self::shopPrice($goodsValue->getPGoodsPrice()),
                'goodsNum' => $goodsValue->getGoodsReturnNum(),
                'goodsReturnAmount' => self::shopPrice($goodsValue->getGoodsReturnAmount()),
            ];
        }

        return self::outJson($templateBody, $array);
    }

    /**
     * 采购入库单数据输出
     * @param array $data
     * @param $templateBody
     * @return string
     */
    public static function purchaseWarehouseOrder(array $data, $templateBody): string
    {
        $array = [
            'companyName' => Common::configValue('base', 'config')['company_name'],
            'warehouseOrderSn' => $data['warehouseOrder']->getWarehouseOrderSn(),
            'paymentCode' => $data['purchaseHelper']->orderPayment($data['orderInfo']->getPaymentCode()),
            'pOrderSn' => $data['orderInfo']->getPOrderSn(),
            'warehouseOrderState' => $data['purchaseHelper']->orderState($data['warehouseOrder']->getWarehouseOrderState()),
            'warehouseOrderInfo' => $data['warehouseOrder']->getWarehouseOrderInfo(),
            'warehouseName' => $data['warehouseOrder']->getOneWarehouse()->getWarehouseName(),
            'supplierName' => $data['orderInfo']->getOneSupplier()->getSupplierName(),
            'supplierContacts' => $data['orderInfo']->getSupplierContacts(),
            'supplierPhone' => $data['orderInfo']->getSupplierPhone(),
            'supplierTelephone' => $data['orderInfo']->getSupplierTelephone(),
            'pOrderInfo' => $data['orderInfo']->getPOrderInfo(),
            'pOrderAmount' => self::shopPrice($data['orderInfo']->getPOrderAmount()),
        ];

        if ($data['warehouseOrder']->getWarehouseOrderState() == 3) {
            foreach ($data['orderGoods'] as $goodsValue) {
                $array['warehouseGoods'][] = [
                    'goodsSn'       => $goodsValue->getGoodsNumber(),
                    'goodsName'     => $goodsValue->getGoodsName(),
                    'goodsSpec'     => $goodsValue->getGoodsSpec(),
                    'goodsUnit'     => $goodsValue->getGoodsUnit(),
                    'warehouseGoodsPrice' => self::shopPrice($goodsValue->getWarehouseGoodsPrice()),
                    'warehouseGoodsBuyNum' => $goodsValue->getWarehouseGoodsBuyNum(),
                    'warehouseGoodsAmount' => self::shopPrice($goodsValue->getWarehouseGoodsAmount()),
                ];
            }
        } else {
            foreach ($data['orderGoods'] as $goodsValue) {
                $array['warehouseGoods'][] = [
                    'goodsSn'       => $goodsValue->getGoodsNumber(),
                    'goodsName'     => $goodsValue->getGoodsName(),
                    'goodsSpec'     => $goodsValue->getGoodsSpec(),
                    'goodsUnit'     => $goodsValue->getGoodsUnit(),
                    'warehouseGoodsPrice' => self::shopPrice($goodsValue->getPGoodsPrice()),
                    'warehouseGoodsBuyNum' => $goodsValue->getPGoodsBuyNum(),
                    'warehouseGoodsAmount' => self::shopPrice($goodsValue->getPGoodsAmount()),
                ];
            }
        }

        return self::outJson($templateBody, $array);
    }

    /**
     * 销售订单数据输出
     * @param array $data
     * @param $templateBody
     * @return string
     */
    public static function salesOrder(array $data, $templateBody): string
    {
        $array = [
            'companyName' => Common::configValue('base', 'config')['company_name'],
            'salesOrderSn' => $data['orderInfo']->getSalesOrderSn(),
            'receivablesCode' => $data['salesHelper']->orderReceivables($data['orderInfo']->getReceivablesCode()),
            'salesOrderState' => $data['salesHelper']->salesOrderState($data['orderInfo']->getSalesOrderState(), 2),
            'customerName' => $data['orderInfo']->getOneCustomer()->getCustomerName(),
            'customerContacts' => $data['orderInfo']->getCustomerContacts(),
            'customerPhone' => $data['orderInfo']->getCustomerPhone(),
            'customerTelephone' => $data['orderInfo']->getCustomerTelephone(),
            'customerAddress' => $data['orderInfo']->getCustomerAddress(),
            'salesOrderInfo' => $data['orderInfo']->getSalesOrderInfo(),
            'salesOrderAmount' => self::shopPrice($data['orderInfo']->getSalesOrderAmount()),
        ];

        foreach ($data['orderGoods'] as $goodsValue) {
            $array['orderGoods'][] = [
                'goodsSn'       => $goodsValue->getGoodsNumber(),
                'goodsName'     => $goodsValue->getGoodsName(),
                'goodsSpec'     => $goodsValue->getGoodsSpec(),
                'goodsUnit'     => $goodsValue->getGoodsUnit(),
                'salesGoodsPrice' => self::shopPrice($goodsValue->getSalesGoodsPrice()),
                'salesGoodsSellNum' => $goodsValue->getSalesGoodsSellNum(),
                'salesGoodsTax' => self::shopPrice($goodsValue->getSalesGoodsTax()),
                'salesGoodsAmount' => self::shopPrice($goodsValue->getSalesGoodsAmount()),
            ];
        }

        foreach ($data['salesOperLog'] as $goodsValue) {
            $array['salesOperLog'][] = [
                'orderState'    => $data['salesHelper']->salesOrderState($goodsValue->getOrderState()),
                'operUser'      => $goodsValue->getOperUser(),
                'operTime'      => date("Y-m-d H:i:s", $goodsValue->getOperTime())
            ];
        }

        return self::outJson($templateBody, $array);
    }

    /**
     * 应付账款单数据输出
     * @param array $data
     * @param $templateBody
     * @return string
     */
    public static function payable(array $data, $templateBody): string
    {
        $array = [
            'companyName' => Common::configValue('base', 'config')['company_name'],
            'warehouseOrderSn' => $data['warehouseOrder']->getWarehouseOrderSn(),
            'pOrderSn' => $data['orderInfo']->getPOrderSn(),
            'warehouseName' => $data['warehouseOrder']->getOneWarehouse()->getWarehouseName(),
            'supplierName' => $data['orderInfo']->getOneSupplier()->getSupplierName(),
            'supplierContacts' => $data['orderInfo']->getSupplierContacts(),
            'supplierPhone' => $data['orderInfo']->getSupplierPhone(),
            'supplierTelephone' => $data['orderInfo']->getSupplierTelephone(),
            'paymentCode' => $data['purchaseHelper']->orderPayment($data['orderInfo']->getPaymentCode()),
            'warehouseOrderState' => $data['purchaseHelper']->orderState($data['warehouseOrder']->getWarehouseOrderState()),
            'warehouseOrderInfo' => $data['warehouseOrder']->getWarehouseOrderInfo(),
            'pOrderInfo' => $data['orderInfo']->getPOrderInfo(),
            'paymentAmount' => self::shopPrice($data['payableInfo']->getPaymentAmount()),
            'finishAmount' => self::shopPrice($data['payableInfo']->getFinishAmount()),
        ];

        if ($data['warehouseOrder']->getWarehouseOrderState() == 3) {
            foreach ($data['orderGoods'] as $goodsValue) {
                $array['orderGoods'][] = [
                    'goodsSn'       => $goodsValue->getGoodsNumber(),
                    'goodsName'     => $goodsValue->getGoodsName(),
                    'goodsSpec'     => $goodsValue->getGoodsSpec(),
                    'goodsUnit'     => $goodsValue->getGoodsUnit(),
                    'goodsPrice' => self::shopPrice($goodsValue->getWarehouseGoodsPrice()),
                    'goodsNum' => $goodsValue->getWarehouseGoodsBuyNum(),
                    'goodsAmount' => self::shopPrice($goodsValue->getWarehouseGoodsAmount()),
                ];
            }
        } else {
            foreach ($data['orderGoods'] as $goodsValue) {
                $array['orderGoods'][] = [
                    'goodsSn'       => $goodsValue->getGoodsNumber(),
                    'goodsName'     => $goodsValue->getGoodsName(),
                    'goodsSpec'     => $goodsValue->getGoodsSpec(),
                    'goodsUnit'     => $goodsValue->getGoodsUnit(),
                    'goodsPrice' => self::shopPrice($goodsValue->getPGoodsPrice()),
                    'goodsNum' => $goodsValue->getPGoodsBuyNum(),
                    'goodsAmount' => self::shopPrice($goodsValue->getPGoodsAmount()),
                ];
            }
        }

        foreach ($data['payableLog'] as $log) {
            $array['payableLog'][] = [
                'payLogPaytime'     => date("Y-m-d", $log->getPayLogPaytime()),
                'payLogAmount'   => self::shopPrice($log->getPayLogAmount()),
                'payLogUser'     => $log->getPayLogUser(),
                'payLogInfo'        => $log->getPayLogInfo(),
                'adminName'             => $log->getOneAdmin()->getAdminName(),
                'payLogAddtime'     => date("Y-m-d H:i:s", $log->getPayLogAddtime())
            ];
        }

        return self::outJson($templateBody, $array);
    }

    /**
     * 应收账款单数据输出
     * @param array $data
     * @param $templateBody
     * @return string
     */
    public static function receivables(array $data, $templateBody): string
    {
        $array = [
            'companyName' => Common::configValue('base', 'config')['company_name'],
            'salesOrderSn' => $data['orderInfo']->getSalesOrderSn(),
            'salesOrderState' => $data['salesHelper']->salesOrderState($data['orderInfo']->getSalesOrderState()),
            'receivablesCode' => $data['salesHelper']->orderReceivables($data['orderInfo']->getReceivablesCode()),
            'customerName' => $data['orderInfo']->getOneCustomer()->getCustomerName(),
            'customerContacts' => $data['orderInfo']->getCustomerContacts(),
            'customerPhone' => $data['orderInfo']->getCustomerPhone(),
            'customerTelephone' => $data['orderInfo']->getCustomerTelephone(),
            'customerAddress' => $data['orderInfo']->getCustomerAddress(),
            'salesOrderInfo' => $data['orderInfo']->getSalesOrderInfo(),
            'salesOrderAmount' => self::shopPrice($data['orderInfo']->getSalesOrderAmount()),
            'finishAmount' => self::shopPrice($data['receivableInfo']->getFinishAmount()),
        ];

        foreach ($data['orderGoods'] as $goodsValue) {
            $array['orderGoods'][] = [
                'goodsSn'       => $goodsValue->getGoodsNumber(),
                'goodsName'     => $goodsValue->getGoodsName(),
                'goodsSpec'     => $goodsValue->getGoodsSpec(),
                'goodsUnit'     => $goodsValue->getGoodsUnit(),
                'salesGoodsPrice' => self::shopPrice($goodsValue->getSalesGoodsPrice()),
                'salesGoodsSellNum' => $goodsValue->getSalesGoodsSellNum(),
                'salesGoodsAmount' => self::shopPrice($goodsValue->getSalesGoodsAmount()),
            ];
        }

        foreach ($data['receivableLog'] as $log) {
            $array['receivableLog'][] = [
                'receivableLogTime'     => date("Y-m-d", $log->getReceivableLogTime()),
                'receivableLogAmount'   => self::shopPrice($log->getReceivableLogAmount()),
                'receivableLogUser'     => $log->getReceivableLogUser(),
                'receivableInfo'        => $log->getReceivableInfo(),
                'adminName'             => $log->getOneAdmin()->getAdminName(),
                'receivableAddTime'     => date("Y-m-d H:i:s", $log->getReceivableAddTime())
            ];
        }

        return self::outJson($templateBody, $array);
    }

    /**
     * 销售退货单数据输出
     * @param array $data
     * @param $templateBody
     * @return string
     */
    public static function salesOrderReturn(array $data, $templateBody): string
    {
        $array = [
            'companyName' => Common::configValue('base', 'config')['company_name'],
            'salesSendOrderSn' => $data['returnInfo']->getSalesSendOrderSn(),
            'salesOrderSn' => $data['returnInfo']->getSalesOrderSn(),
            'returnState' => $data['salesHelper']->salesOrderReturnState($data['returnInfo']->getReturnState()),
            'receivablesCode' => $data['salesHelper']->orderReceivables($data['returnInfo']->getOneSalesOrder()->getReceivablesCode()),
            'customerName' => $data['returnInfo']->getOneSalesOrder()->getOneCustomer()->getCustomerName(),
            'customerContacts' => $data['returnInfo']->getOneSalesOrder()->getCustomerContacts(),
            'customerPhone' => $data['returnInfo']->getOneSalesOrder()->getCustomerPhone(),
            'customerTelephone' => $data['returnInfo']->getOneSalesOrder()->getCustomerTelephone(),
            'customerAddress' => $data['returnInfo']->getOneSalesOrder()->getCustomerAddress(),
            'salesOrderReturnInfo' => $data['returnInfo']->getSalesOrderReturnInfo(),
            'salesOrderReturnAmount' => self::shopPrice($data['returnInfo']->getSalesOrderReturnAmount()),
        ];

        foreach ($data['orderGoods'] as $goodsValue) {
            $array['orderGoods'][] = [
                'goodsSn'       => $goodsValue->getGoodsNumber(),
                'goodsName'     => $goodsValue->getGoodsName(),
                'goodsSpec'     => $goodsValue->getGoodsSpec(),
                'goodsUnit'     => $goodsValue->getGoodsUnit(),
                'salesGoodsPrice' => self::shopPrice($goodsValue->getSalesGoodsPrice()),
                'goodsReturnNum' => $goodsValue->getGoodsReturnNum(),
                'returnAmount' => self::shopPrice($goodsValue->getGoodsReturnAmount())
            ];
        }

        return self::outJson($templateBody, $array);
    }

    /**
     * 销售发货单数据输出
     * @param array $data
     * @param $templateBody
     * @return string
     */
    public static function salesSendOrder(array $data, $templateBody): string
    {
        $array = [
            'companyName' => Common::configValue('base', 'config')['company_name'],
            'sendOrderSn' => $data['sendOrderInfo']->getSendOrderSn(),
            'salesOrderSn' => $data['sendOrderInfo']->getOneSalesOrder()->getSalesOrderSn(),
            'salesOrderState' => $data['salesHelper']->salesOrderState($data['sendOrderInfo']->getOneSalesOrder()->getSalesOrderState()),
            'receivablesCode' => $data['salesHelper']->orderReceivables($data['sendOrderInfo']->getOneSalesOrder()->getReceivablesCode()),
            'customerName' => $data['sendOrderInfo']->getOneSalesOrder()->getOneCustomer()->getCustomerName(),
            'customerContacts' => $data['sendOrderInfo']->getOneSalesOrder()->getCustomerContacts(),
            'customerPhone' => $data['sendOrderInfo']->getOneSalesOrder()->getCustomerPhone(),
            'customerTelephone' => $data['sendOrderInfo']->getOneSalesOrder()->getCustomerTelephone(),
            'customerAddress' => $data['sendOrderInfo']->getOneSalesOrder()->getCustomerAddress(),
            'salesOrderInfo' => $data['sendOrderInfo']->getOneSalesOrder()->getSalesOrderInfo(),
            'salesOrderAmount' => self::shopPrice($data['sendOrderInfo']->getOneSalesOrder()->getSalesOrderAmount()),
        ];

        foreach ($data['orderGoods'] as $key => $goodsValue) {
            $array['warehouseGoods'][$key] = [
                'goodsSn'       => $goodsValue->getGoodsNumber(),
                'goodsName'     => $goodsValue->getGoodsName(),
                'goodsSpec'     => $goodsValue->getGoodsSpec(),
                'goodsUnit'     => $goodsValue->getGoodsUnit(),
                'salesGoodsPrice' => self::shopPrice($goodsValue->getSalesGoodsPrice()),
                'salesGoodsSellNum' => $goodsValue->getSalesGoodsSellNum(),
                'salesGoodsTax' => self::shopPrice($goodsValue->getSalesGoodsTax()),
                'salesGoodsAmount' => self::shopPrice($goodsValue->getSalesGoodsAmount()),
            ];
            if ($data['sendWarehouse'][$goodsValue->getGoodsId()]) {
                foreach ($data['sendWarehouse'][$goodsValue->getGoodsId()] as $value) {
                    $array['warehouseGoods'][$key]['warehouseName'][] = $value['warehouseName']. '(发货数量:' .$value['goodsNum']. ')';
                }
            }
        }

        return self::outJson($templateBody, $array);
    }

    /**
     * 商城订单数据输出
     * @param array $data
     * @param $templateBody
     * @return string
     */
    public static function shopOrder(array $data, $templateBody): string
    {
        $array = [
            'companyName' => Common::configValue('base', 'config')['company_name'],
            'shopOrderSn' => $data['orderInfo']->getShopOrderSn(),
            'shopOrderState' => $data['shop']->shopOrderState($data['orderInfo']->getShopOrderState()),
            'shopPaymentName' => $data['orderInfo']->getShopPaymentName(),
            'shopOrderAddTime' => date("Y-m-d H:i:s", $data['orderInfo']->getShopOrderAddTime()),
            'shopOrderPayTime' => !empty($data['orderInfo']->getShopOrderPayTime()) ? date("Y-m-d H:i:s", $data['orderInfo']->getShopOrderPayTime()) : '暂无',
            'shopOrderExpressTime' => !empty($data['orderInfo']->getShopOrderExpressTime()) ? date("Y-m-d H:i:s", $data['orderInfo']->getShopOrderExpressTime()) : '暂无',
            'shopOrderFinishTime' => !empty($data['orderInfo']->getShopOrderFinishTime()) ? date("Y-m-d H:i:s", $data['orderInfo']->getShopOrderFinishTime()) : '暂无',
            'shopOrderMessage' => !empty($data['orderInfo']->getShopOrderMessage()) ? $data['orderInfo']->getShopOrderMessage() : '暂无',
            'shopPaymentCost' => $data['orderInfo']->getShopPaymentCost() > 0 ? self::shopPrice($data['orderInfo']->getShopPaymentCost()) : '0',
            'shopExpressCost' => $data['orderInfo']->getShopExpressCost() > 0 ? self::shopPrice($data['orderInfo']->getShopExpressCost()) : '0',
            'shopOrderOtherInfo' => $data['orderInfo']->getShopOrderOtherCost() > 0 ? self::shopPrice($data['orderInfo']->getShopOrderOtherCost()) : '0',
            'shopOrderDiscountInfo' => $data['orderInfo']->getShopOrderDiscountAmount() > 0 ? self::shopPrice($data['orderInfo']->getShopOrderDiscountAmount()) : '0',
            'shopOrderAmount' => self::shopPrice($data['orderInfo']->getShopOrderAmount()),
        ];

        foreach ($data['orderGoods'] as $goodsValue) {
            $array['orderGoods'][] = [
                'goodsSn'       => $goodsValue->getGoodsSn(),
                'goodsName'     => $goodsValue->getGoodsName(),
                'goodsSpec'     => $goodsValue->getGoodsSpec(),
                'goodsUnit'     => $goodsValue->getGoodsUnitName(),
                'goodsPrice' => self::shopPrice($goodsValue->getGoodsPrice()),
                'goodsNum' => $goodsValue->getBuyNum(),
                'goodsAmount' => self::shopPrice($goodsValue->getGoodsAmount())
            ];
        }

        $array['shopExpress'][] = [
            'shopExpressName'=> $data['orderInfo']->getShopExpressName(),
            'deliveryNumber'=> $data['deliveryInfo']->getDeliveryNumber(),
            'deliveryName'  => $data['deliveryInfo']->getDeliveryName(),
            'deliveryPhone' => $data['deliveryInfo']->getDeliveryPhone(),
            'deliveryTelephone'=> $data['deliveryInfo']->getDeliveryTelephone(),
            'zipCode'       => $data['deliveryInfo']->getZipCode(),
            'regionAddress' => $data['deliveryInfo']->getRegionInfo() . $data['deliveryInfo']->getRegionAddress(),
            'deliveryInfo'  => $data['deliveryInfo']->getDeliveryInfo()
        ];

        return self::outJson($templateBody, $array);
    }

    /**
     * @param $templateBody
     * @param $array
     * @return string
     */
    public static function outJson($templateBody, $array): string
    {
        $templateBodyArray = (array) Json::decode($templateBody);

        $printData = [];
        if (isset($templateBodyArray['panels'][0]->printElements) && !empty($templateBodyArray['panels'][0]->printElements)) foreach ($templateBodyArray['panels'][0]->printElements as $value) {
            if (in_array($value->tid, ['customText', 'customLongText', 'hline', 'vline', 'rect', 'oval'])) continue;

            if (isset($array[$value->tid])) {
                $printData[$value->tid] = $array[$value->tid];
            }
        }

        return Json::encode($printData);
    }

    /**
     * 价格转换
     * @param $price
     * @return int|string
     */
    public static function shopPrice($price)
    {
        if ($price == 0.00 || $price == 0) return '0';

        $currency = new CurrencyFormat();
        return $currency($price, Common::configValue('base', 'config')['currency_code'], 2, 'zh_CN');
    }
}