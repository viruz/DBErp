<?php
/**
 * DBERP 进销存系统
 *
 * ==========================================================================
 * @link      http://www.dberp.net/
 * @copyright 北京珑大钜商科技有限公司，并保留所有权利。
 * @license   http://www.dberp.net/license.html License
 * ==========================================================================
 *
 * @author    静静的风 <baron@loongdom.cn>
 *
 */

namespace Sales\Service;

use Doctrine\ORM\EntityManager;
use Sales\Entity\SalesOrder;
use Sales\Entity\SalesOrderGoods;
use Sales\Entity\SalesSendOrder;
use Sales\Entity\SalesSendWarehouseGoods;
use Store\Entity\GoodsSerialNumber;
use Store\Entity\Warehouse;
use Store\Entity\WarehouseGoods;

class SalesSendWarehouseGoodsManager
{
    private $entityManager;

    public function __construct(
        EntityManager $entityManager
    )
    {
        $this->entityManager = $entityManager;
    }

    /**
     * 添加仓库发货商品
     * @param array $data
     * @param SalesSendOrder $sendOrder
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function addSalesSendWarehouseGoods(array $data, SalesSendOrder $sendOrder)
    {
        $nowTime = time();

        foreach ($data as $value) {
            $oneWarehouse = $this->entityManager->getRepository(Warehouse::class)->findOneByWarehouseId($value['warehouseId']);

            $sendWarehouseGoods = new SalesSendWarehouseGoods();
            $sendWarehouseGoods->setSendWarehouseGoodsId(null);
            $sendWarehouseGoods->setSalesOrderId($sendOrder->getSalesOrderId());
            $sendWarehouseGoods->setSendOrderId($sendOrder->getSendOrderId());
            $sendWarehouseGoods->setGoodsId($value['goodsId']);
            $sendWarehouseGoods->setWarehouseId($value['warehouseId']);
            $sendWarehouseGoods->setSendGoodsStock($value['sendNum']);
            $sendWarehouseGoods->setOneWarehouse($oneWarehouse);

            $this->entityManager->persist($sendWarehouseGoods);
            $this->entityManager->flush();

            if ($value['goodsSerialNumberState'] == 1) {
                $serialNumberArray = [];
                if (!empty($value['serialNumberArray'])) {
                    foreach ($value['serialNumberArray'] as $serialNumberValue) {
                        $serialNumberInfo = $this->entityManager->getRepository(GoodsSerialNumber::class)->findOneBy(['serialNumber' => $serialNumberValue, 'goodsId' => $value['goodsId'], 'serialNumberState' => 1]);
                        if ($serialNumberInfo) {
                            $serialNumberArray[] = $serialNumberInfo->getSerialNumber();

                            $serialNumberInfo->setWarehouseId($value['warehouseId']);
                            $serialNumberInfo->setSerialNumberState(2);
                            $serialNumberInfo->setSerialNumberType(4);//销售出库
                            $serialNumberInfo->setOutboundInId($sendOrder->getSalesOrderId());
                            $serialNumberInfo->setOutboundTime($nowTime);

                            $this->entityManager->flush();
                        }
                    }
                } else {
                    $goodsSerialNumberList = $this->entityManager->getRepository(GoodsSerialNumber::class)->findBy(['goodsId' => $value['goodsId'], 'serialNumberState' => 1]);
                    foreach ($goodsSerialNumberList as $serialKey => $serialValue) {
                        if (($serialKey + 1) > $value['sendNum']) break;

                        $serialNumberArray[] = $serialValue->getSerialNumber();

                        $serialValue->setWarehouseId($value['warehouseId']);
                        $serialValue->setSerialNumberState(2);
                        $serialValue->setSerialNumberType(4);//销售出库
                        $serialValue->setOutboundInId($sendOrder->getSalesOrderId());
                        $serialValue->setOutboundTime($nowTime);

                        $this->entityManager->flush();
                    }
                }

                if (!empty($serialNumberArray)) {
                    $sendWarehouseGoods->setGoodsSerialNumberStr(implode(',', $serialNumberArray));
                    $this->entityManager->flush();
                }
            }
        }
    }

    /**
     * 检查并获取仓库需要的出库商品
     * @param array $data
     * @param $salesOrderGoods
     * @return array
     */
    public function checkAndReturnSendWarehouseGoodsNum(array $data, $salesOrderGoods): array
    {
        $sendWarehouseGoods = [];
        $goodsStock = [];
        foreach ($salesOrderGoods as $goodsValue) {
            if(count($data['sendWarehouse'][$goodsValue->getGoodsId()]) > 1) rsort($data['sendWarehouse'][$goodsValue->getGoodsId()]);
            $goodsStock[$goodsValue->getGoodsId()] = $goodsValue->getSalesGoodsSellNum();//获取商品需要减少的库存
            $salesGoodsNum= $goodsValue->getSalesGoodsSellNum();

            if (!empty($data['inputSerialNumberStr'][$goodsValue->getGoodsId()]) && $goodsValue->getGoodsSerialNumberState() == 1) {//手动添加商品发货序列号
                $goodsSerialNumberArray = array_filter(explode(',', $data['inputSerialNumberStr'][$goodsValue->getGoodsId()]));
                $serialNumberList   = $this->entityManager->getRepository(GoodsSerialNumber::class)->findGoodsSerialNumberBySerialNumber($goodsSerialNumberArray, $goodsValue->getGoodsId());
                $warehouseArray     = [];
                $warehouseSerialArray = [];
                foreach ($serialNumberList as $serialValue) {
                    $warehouseArray[$serialValue->getWarehouseId()]['sendNum'] = !empty($warehouseArray[$serialValue->getWarehouseId()]['sendNum']) ? $warehouseArray[$serialValue->getWarehouseId()]['sendNum'] + 1 : 1;
                    $warehouseSerialArray[$serialValue->getWarehouseId()][] = $serialValue->getSerialNumber();
                }
                foreach ($warehouseArray as $key => $value) {
                    $sendWarehouseGoods[] = ['warehouseId' => $key, 'goodsId' => $goodsValue->getGoodsId(), 'sendNum' => $value['sendNum'], 'serialNumberArray' => $warehouseSerialArray[$key], 'goodsSerialNumberState' => 1];
                }
            } else {
                foreach ($data['sendWarehouse'][$goodsValue->getGoodsId()] as $warehouseId) {
                    $warehouseGoods = $this->entityManager->getRepository(WarehouseGoods::class)->findOneBy(['warehouseId' => $warehouseId, 'goodsId' => $goodsValue->getGoodsId()]);
                    //在第一个仓库如果已经满足发货要求，则直接在第一个仓库出货
                    if($warehouseGoods->getWarehouseGoodsStock() >= $salesGoodsNum) {
                        $sendWarehouseGoods[] = ['warehouseId' => $warehouseId, 'goodsId' => $goodsValue->getGoodsId(), 'sendNum' => $salesGoodsNum, 'goodsSerialNumberState' => $goodsValue->getGoodsSerialNumberState()];
                        break;
                    } else {
                        $sendWarehouseGoods[] = ['warehouseId' => $warehouseId, 'goodsId' => $goodsValue->getGoodsId(), 'sendNum' => $warehouseGoods->getWarehouseGoodsStock(), 'goodsSerialNumberState' => $goodsValue->getGoodsSerialNumberState()];
                        $salesGoodsNum = $salesGoodsNum - $warehouseGoods->getWarehouseGoodsStock();
                    }
                }
            }
        }
        return ['goods' => $goodsStock, 'warehouseGoods' => $sendWarehouseGoods];
    }
}