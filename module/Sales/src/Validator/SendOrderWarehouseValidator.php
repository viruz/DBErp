<?php
/**
 * DBERP 进销存系统
 *
 * ==========================================================================
 * @link      http://www.dberp.net/
 * @copyright 北京珑大钜商科技有限公司，并保留所有权利。
 * @license   http://www.dberp.net/license.html License
 * ==========================================================================
 *
 * @author    静静的风 <baron@loongdom.cn>
 *
 */

namespace Sales\Validator;

use Laminas\I18n\Translator\Translator;
use Store\Entity\GoodsSerialNumber;
use Store\Entity\WarehouseGoods;
use Laminas\Validator\AbstractValidator;

class SendOrderWarehouseValidator extends AbstractValidator
{
    const NOT_SCALAR            = 'notScalar';
    const WAREHOUSE_NOT_STOCK   = 'warehouseNotStock';
    const GOODS_SERIAL_NUMBER_ERROR = 'goodsSerialNumberError';

    protected $messageTemplates = [];

    private $entityManager;
    private $sendOrderGoods;

    public function __construct($options)
    {
        $this->entityManager    = $options['entityManager'];
        $this->sendOrderGoods   = $options['sendOrderGoods'];

        $trans = new Translator();
        $this->messageTemplates = [
            self::NOT_SCALAR                    => $trans->translate("这不是一个标准输入值"),
            self::WAREHOUSE_NOT_STOCK           => $trans->translate("库存不足，无法发货"),
            self::GOODS_SERIAL_NUMBER_ERROR     => $trans->translate("无足够的商品序列号出库"),
        ];

        parent::__construct($options);
    }

    public function isValid($value)
    {
        if (!is_array($value)) {
            $this->error(self::NOT_SCALAR);
            return false;
        }

        if(empty($this->sendOrderGoods)) return false;

        foreach ($this->sendOrderGoods as $goodsValue) {
            if(!isset($value[$goodsValue->getGoodsId()])) {
                $this->error(self::WAREHOUSE_NOT_STOCK);
                return false;
            }
            $stockNum = $this->entityManager->getRepository(WarehouseGoods::class)->findMoreWarehouseGoodsNum($value[$goodsValue->getGoodsId()], $goodsValue->getGoodsId());
            if($goodsValue->getSalesGoodsSellNum() > $stockNum) {
                $this->error(self::WAREHOUSE_NOT_STOCK);
                return false;
            }

            if ($goodsValue->getGoodsSerialNumberState() == 1) {
                $goodsSerialCount = $this->entityManager->getRepository(GoodsSerialNumber::class)->count(['goodsId' => $goodsValue->getGoodsId(), 'serialNumberState' => 1]);
                if ($goodsSerialCount < $goodsValue->getSalesGoodsSellNum()) {
                    $this->error(self::GOODS_SERIAL_NUMBER_ERROR);
                    return false;
                }
            }
        }

        return true;
    }
}