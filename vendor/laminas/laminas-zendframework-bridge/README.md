# laminas-zendframework-bridge

[![Build Status](https://github.com/laminas/laminas-zendframework-bridge/workflows/Continuous%20Integration/badge.svg)](https://github.com/laminas/laminas-zendframework-bridge/actions?query=workflow%3A"Continuous+Integration")

> This package is considered feature-complete, and is now in **security-only** maintenance mode, following a [decision by the Technical Steering Committee](https://github.com/laminas/technical-steering-committee/blob/6dc4d8a798021f690ad8c65d0d9a0c4b21835a1e/meetings/minutes/2022-07-11-TSC-Minutes.md#future-of-the-migration-layer).
> If you have a security issue, please [follow our security reporting guidelines](https://getlaminas.org/security/).
> If you wish to take on the role of maintainer, please [nominate yourself](https://github.com/laminas/technical-steering-committee/issues/new?assignees=&labels=Nomination&template=Maintainer_Nomination.md&title=%5BNOMINATION%5D%5BMAINTAINER%5D%3A+%7Bname+of+person+being+nominated%7D)



This library provides a custom autoloader that aliases legacy Zend Framework,
Apigility, and Expressive classes to their replacements under the Laminas
Project.

This package should be installed only if you are also using the composer plugin
that installs Laminas packages to replace ZF/Apigility/Expressive packages.

This tool supports:

* Zend Framework MVC projects, all v2 and v3 releases
* Apigility projects, all stable versions
* Expressive versions, all stable versions

## Installation

Run the following to install this library:

```bash
$ composer require laminas/laminas-zendframework-bridge
```

## Configuration

- Since 1.6.0

You may provide additional replacements for the configuration post processor.
This is particularly useful if your application uses third-party components that include class names that the post processor otherwise rewrites, and which you want to never rewrite.

Configuration is via the following structure:

```php
return [
    'laminas-zendframework-bridge' => [
        'replacements' => [
            'to-replace' => 'replacement',
            // ...
        ],
    ],
];
```

As an example, if your configuration included the following dependency mapping:

```php
return [
    'controller_plugins' => [
        'factories' => [
            'customZendFormBinder' => \CustomZendFormBinder\Controller\Plugin\Factory\BinderPluginFactory::class,
        ],
    ],
];
```

And you wanted the two strings that contain the verbiage `ZendForm` to remain untouched, you could define the following replacements mapping:

```php
return [
    'laminas-zendframework-bridge' => [
        'replacements' => [
            // Never rewrite!
            'customZendFormBinder' => 'customZendFormBinder',
            'CustomZendFormBinder' => 'CustomZendFormBinder',
        ],
    ],
];
```

## Support

* [Issues](https://github.com/laminas/laminas-zendframework-bridge/issues/)
* [Forum](https://discourse.laminas.dev/)
